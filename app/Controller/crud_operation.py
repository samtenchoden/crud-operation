from os import name
from app import app,db
from sqlalchemy import engine
from sqlalchemy.engine import create_engine
from app import db_connection
from flask import json, jsonify, request


@app.route('/insert',methods=['POST'])
def insert():
    jsonData = request.json
    print(jsonData)
    std_id = jsonData['stdid']
    std_name = jsonData['stdname']
    std_email = jsonData['email']
    std_age = jsonData['age']

    db.engine.execute("INSERT INTO student_details(std_id, std_name,std_email,std_age) VALUES ("+str(std_id)+",'"+std_name+"','"+std_email+"',"+str(std_age)+")")
    return jsonify({'message': 'successfully added'})

@app.route('/update',methods=['PUT'])
def update():
    jsonData = request.json
    print(jsonData)
    std_id = jsonData['stdid']
    std_name = jsonData['stdname']
    std_email = jsonData['email']
    std_age = jsonData['age']

    db.engine.execute("UPDATE student_details SET std_id = "+str(std_id)+",std_name='"+std_name+"',std_email='"+std_email+"',std_age="+str(std_age)+" WHERE std_id = "+str(std_id))
    return jsonify({'message': 'successfully updated'})

@app.route('/show', methods=['GET'])
def show():
    data = db.engine.execute("SELECT * FROM student_details")
    return jsonify([dict(row) for row in data ])

@app.route('/delete/<int:std_id>', methods=['DELETE'])
def delete(std_id):
    db.engine.execute("DELETE FROM student_details WHERE std_id="+str(std_id))
    return jsonify({'message':'deleted successfully'})
    