from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import engine
from sqlalchemy.engine import create_engine
from sqlalchemy_utils import database_exists, create_database
#set db connection
db_connection = f'mysql+pymysql://root:root@127.0.0.1:3306/crud_operation'

app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URL'] = db_connection

app.config['SQLALCHEMY_DATABASE_URI'] = db_connection
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

engine = create_engine(db_connection)

if not database_exists(engine.url):
    create_database(engine.url)
else:
    engine.connect()

from app.Controller import crud_operation

db.create_all()